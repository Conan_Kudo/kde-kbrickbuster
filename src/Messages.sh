#! /usr/bin/env bash
$EXTRACTRC `find . -name \*.rc -o -name \*.ui -o -name \*.kcfg` >> rc.cpp
$XGETTEXT `find . -name \*.cpp` -o $podir/kbrickbuster.pot
$XGETTEXT `find . -name \*.qml -o -name \*.js` -j -L Java -o $podir/kbrickbuster.pot
rm -f rc.cpp
