/*
    SPDX-FileCopyrightText: 2007-2008 Fela Winkelmolen <fela.kde@gmail.com>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

// own
#include "mainwindow.h"
#include "kbrickbuster_version.h"
// KF
#include <KAboutData>
#include <KCrash>
#include <KLocalizedString>
#include <KDBusService>
// Qt
#include <QApplication>
#include <QCommandLineParser>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    KLocalizedString::setApplicationDomain("kbrickbuster");

    KAboutData aboutData(QStringLiteral("kbrickbuster"), i18n("KBrickBuster"), QStringLiteral(KBREAKOUT_VERSION_STRING));
    aboutData.setShortDescription(i18n("A BrickBuster like game by KDE"));
    aboutData.setLicense(KAboutLicense::GPL);
    aboutData.setCopyrightStatement(i18n("(c) 2007-2008 Fela Winkelmolen"));
    aboutData.addAuthor(i18n("Fela Winkelmolen"),
                        i18n("original author and maintainer"),
                        QStringLiteral("fela.kde@gmail.com"));
    aboutData.addAuthor(i18n("Eugene Trounev"),
                        i18n("artwork"),
                        QStringLiteral("eugene.trounev@gmail.com"));
    aboutData.addAuthor(i18n("Sean Wilson"),
                        i18n("artwork"),
                        QStringLiteral("suseux@gmail.com"));
    aboutData.addCredit(i18n("Lorenzo Bonomi"),
                        i18n("testing"),
                        QStringLiteral("lorenzo.bonomi@hotmail.it"));
    aboutData.addCredit(i18n("Brian Croom"),
                        i18n("port to KGameRenderer"),
                        QStringLiteral("brian.s.croom@gmail.com"));
    aboutData.addCredit(i18n("Viranch Mehta"),
                        i18n("port to QtQuick"),
                        QStringLiteral("viranch.mehta@gmail.com"));
    aboutData.setHomepage(QStringLiteral("https://apps.kde.org/kbrickbuster"));

    QCommandLineParser parser;
    KAboutData::setApplicationData(aboutData);
    KCrash::initialize();
    aboutData.setupCommandLine(&parser);
    parser.process(app);
    aboutData.processCommandLine(&parser);

    app.setWindowIcon(QIcon::fromTheme(QStringLiteral("kbrickbuster")));

    KDBusService service;
    MainWindow *window = new MainWindow();
    Q_UNUSED(window);

    return app.exec();
}

